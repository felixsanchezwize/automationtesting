import { Selector } from 'testcafe';

fixture `AirAsia TestCafe pipeline POC`
    .page `https://deals.airasia.com/`;

test('Click on Kuala Lumpur popular activities', async t => {
    await t
        .click(Selector('body > main > div > section.container.pt-3.pt-sm-5 > div.d-none.d-sm-block.mt-3 > div:nth-child(1) > div:nth-child(1) > a > div'))
        .expect(Selector('h2.d-none').innerText).eql('POPULAR ACTIVITIES IN  KUALA LUMPUR');
});

test('Click on Singapore popular activities', async t => {
    await t
        .click(Selector('body > main > div > section.container.pt-3.pt-sm-5 > div.d-none.d-sm-block.mt-3 > div:nth-child(1) > div:nth-child(2) > a > div'))
        .expect(Selector('h2.d-none').innerText).eql('POPULAR ACTIVITIES IN  SINGAPORE');
});

test('Click on Hong Kong popular activities', async t => {
    await t
        .click(Selector('body > main > div > section.container.pt-3.pt-sm-5 > div.d-none.d-sm-block.mt-3 > div:nth-child(1) > div:nth-child(3) > a > div'))
        .expect(Selector('h2.d-none').innerText).eql('POPULAR ACTIVITIES IN  HONG KONG');
});